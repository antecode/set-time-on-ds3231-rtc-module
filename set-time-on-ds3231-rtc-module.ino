#include <DS3231.h>
DS3231  rtc(SDA, SCL);

void setup()
{
  Serial.begin(9600);
  rtc.begin(); 
 //The following lines can be uncommented to set the date and time
 rtc.setDOW(THURSDAY);     // Set Day-of-Week to SUNDAY
 rtc.setTime(10, 23, 40);     // Set the time to 12:00:00 (24hr format)
 rtc.setDate(22, 11, 2018);   // Set the date to January 1st, 2014

Serial.print("Time:  ");
Serial.print("\t\t");
Serial.println("Date: ");
}
void loop() { 
Serial.print(rtc.getTimeStr());
Serial.print("\t"); 
Serial.println(rtc.getDateStr());
 
delay(3000); 
}
