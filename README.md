# set time on DS3231 RTC module

simple code to set correct time on DS3231 RTC module
- download and install DS3231 library from https://github.com/rodan/ds3231
- connect
rtc   Pins
-----------
Vcc - 3.3v
Gnd - Gnd
SDA - A4
SCL - A5

- edit lines 9,10,11 to your current time
- upload code to board (arduino UNO)
- done